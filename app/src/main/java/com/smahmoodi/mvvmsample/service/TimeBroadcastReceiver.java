package com.smahmoodi.mvvmsample.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.smahmoodi.mvvmsample.ClockApp;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeBroadcastReceiver extends BroadcastReceiver {
    private final SimpleDateFormat _sdfWatchTime = new SimpleDateFormat("HH:mm");

    /*@Inject
    AppService appService ;
*/

    //ClockViewModel clockViewModel;

    public TimeBroadcastReceiver(ClockApp app) {
        super();
        //clockViewModel=ClockViewModel.getInstance(app);
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        //if (intent.getAction().compareTo(Intent.ACTION_TIME_TICK) == 0)
        //clockViewModel.onPostClockUpdated(_sdfWatchTime.format(new Date()));

        Toast.makeText(context, "fired:" + _sdfWatchTime.format(new Date()), Toast.LENGTH_SHORT).show();

        Log.e("TimeBroadcastReceiver","received");
    }


}
