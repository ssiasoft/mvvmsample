package com.smahmoodi.mvvmsample.model;

import android.databinding.Bindable;
import android.databinding.PropertyChangeRegistry;

import com.smahmoodi.mvvmsample.BR;

public class ObservableClock  implements android.databinding.Observable {
    private PropertyChangeRegistry registry = new PropertyChangeRegistry();


    private String now;

    @Bindable public String getNow(){
        return now;
    }

    public void setNow(String now){
        this.now = now;
        registry.notifyChange(this, BR.now);
    }


    @Override
    public void addOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        registry.add(callback);
    }

    @Override
    public void removeOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        registry.remove(callback);
    }



}
