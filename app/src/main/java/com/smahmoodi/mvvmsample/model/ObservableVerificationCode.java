package com.smahmoodi.mvvmsample.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.PropertyChangeRegistry;

import com.smahmoodi.mvvmsample.BR;
import com.smahmoodi.mvvmsample.R;

import java.util.Objects;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class ObservableVerificationCode extends BaseObservable implements android.databinding.Observable {
    private PropertyChangeRegistry registry = new PropertyChangeRegistry();


    private String verificationCode = "";
    private String message = "";
    private int messageColor = R.color.yellow;
    private boolean loading = false;


    @Bindable public String getVerificationCode(){
        return verificationCode;
    }
    @Bindable public String getMessage(){
        return message;
    }
    @Bindable public int getColor(){
        return messageColor;
    }
    @Bindable public int getLoading() {
        return loading ? VISIBLE : GONE;
    }

    public void setVerificationCode(String verificationCode){
        if (!Objects.equals(this.verificationCode, verificationCode)) {
            this.verificationCode = verificationCode;
            registry.notifyChange(this, BR.verificationCode);
        }

    }

    public void setMessageColor(int color){
        if (!Objects.equals(this.messageColor, color)) {
            this.messageColor = color;
            registry.notifyChange(this, BR.color);
        }

    }

    public void setLoading(boolean loading) {
        if (!Objects.equals(this.loading, loading)) {
            this.loading = loading;
            registry.notifyChange(this, BR.loading);
        }

    }


    public void setMessage(String message){
        if (!Objects.equals(this.message, message)) {
            this.message = message;
            registry.notifyChange(this, BR.message);
        }

    }

    @Override
    public void addOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        registry.add(callback);
    }

    @Override
    public void removeOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        registry.remove(callback);
    }



}
