package com.smahmoodi.mvvmsample.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.PropertyChangeRegistry;

import com.smahmoodi.mvvmsample.BR;
import com.smahmoodi.mvvmsample.R;

import java.util.Objects;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class ObservableRegister extends BaseObservable implements android.databinding.Observable {
    private PropertyChangeRegistry registry = new PropertyChangeRegistry();


    private String mobilePhoneNumber = "";
    private String password = "";
    private String firstname = "";
    private String lastname = "";
    private String message = "";
    private int messageColor = R.color.yellow;
    private boolean loading = false;


    @Bindable public String getMobilePhoneNumber(){
        return mobilePhoneNumber;
    }
    @Bindable public String getLastname(){
        return lastname;
    }
    @Bindable public String getFirstname(){
        return firstname;
    }
    @Bindable public String getMessage(){
        return message;
    }
    @Bindable public String getPassword(){
        return password;
    }
    @Bindable public int getColor(){
        return messageColor;
    }
    @Bindable public int getLoading() {
        return loading ? VISIBLE : GONE;
    }

    public void setMobilePhoneNumber(String mobilePhoneNumber){
        if (!Objects.equals(this.mobilePhoneNumber, mobilePhoneNumber)) {
            this.mobilePhoneNumber = mobilePhoneNumber;
            registry.notifyChange(this, BR.username);
        }

    }

    public void setFirstname(String firstname){
        if (!Objects.equals(this.firstname, firstname)) {
            this.firstname = firstname;
            registry.notifyChange(this, BR.firstname);
        }

    }

    public void setLastname(String lastname){
        if (!Objects.equals(this.lastname, lastname)) {
            this.lastname = lastname;
            registry.notifyChange(this, BR.lastname);
        }

    }

    public void setMessageColor(int color){
        if (!Objects.equals(this.messageColor, color)) {
            this.messageColor = color;
            registry.notifyChange(this, BR.color);
        }

    }

    public void setLoading(boolean loading) {
        if (!Objects.equals(this.loading, loading)) {
            this.loading = loading;
            registry.notifyChange(this, BR.loading);
        }

    }


    public void setMessage(String message){
        if (!Objects.equals(this.message, message)) {
            this.message = message;
            registry.notifyChange(this, BR.message);
        }

    }

    public void setPassword(String password){
        if (!Objects.equals(this.password, password)) {
            this.password = password;
            registry.notifyChange(this, BR.password);
        }
    }

    @Override
    public void addOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        registry.add(callback);
    }

    @Override
    public void removeOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        registry.remove(callback);
    }



}
