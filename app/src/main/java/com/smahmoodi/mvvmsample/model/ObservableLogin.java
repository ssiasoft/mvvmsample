package com.smahmoodi.mvvmsample.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.PropertyChangeRegistry;

import com.smahmoodi.mvvmsample.BR;
import com.smahmoodi.mvvmsample.R;

import java.util.Objects;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class ObservableLogin extends BaseObservable implements android.databinding.Observable {
    private PropertyChangeRegistry registry = new PropertyChangeRegistry();


    private String username = "";
    private String password = "";
    private String message = "";
    private int messageColor = R.color.yellow;
    private boolean loading = false;


    @Bindable public String getUsername(){
        return username;
    }
    @Bindable public String getMessage(){
        return message;
    }
    @Bindable public String getPassword(){
        return password;
    }
    @Bindable public int getColor(){
        return messageColor;
    }
    @Bindable public int getLoading() {
        return loading ? VISIBLE : GONE;
    }

    public void setUsername(String username){
        if (!Objects.equals(this.username, username)) {
            this.username = username;
            registry.notifyChange(this, BR.username);
        }

    }

    public void setMessageColor(int color){
        if (!Objects.equals(this.messageColor, color)) {
            this.messageColor = color;
            registry.notifyChange(this, BR.color);
        }

    }

    public void setLoading(boolean loading) {
        if (!Objects.equals(this.loading, loading)) {
            this.loading = loading;
            registry.notifyChange(this, BR.loading);
        }

    }


    public void setMessage(String message){
        if (!Objects.equals(this.message, message)) {
            this.message = message;
            registry.notifyChange(this, BR.message);
        }

    }

    public void setPassword(String password){
        if (!Objects.equals(this.password, password)) {
            this.password = password;
            registry.notifyChange(this, BR.password);
        }
    }

    @Override
    public void addOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        registry.add(callback);
    }

    @Override
    public void removeOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        registry.remove(callback);
    }



}
