package com.smahmoodi.mvvmsample.ui.interfaces;

public interface Callback {

    void onFragmentAttached();

    void onFragmentDetached(String tag);
}