package com.smahmoodi.mvvmsample.ui.login;

public interface LoginActions{
    void login();

    void loggedIn();

    void handleError(Throwable throwable);

    void setLoading(boolean b);

    void toRegister();

    void toForgetPass();
}
