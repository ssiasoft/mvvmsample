package com.smahmoodi.mvvmsample.ui.login;

import android.os.Bundle;

import com.smahmoodi.mvvmsample.BR;
import com.smahmoodi.mvvmsample.R;
import com.smahmoodi.mvvmsample.data.api.AppApiHelper;
import com.smahmoodi.mvvmsample.databinding.ActivityLoginBinding;
import com.smahmoodi.mvvmsample.model.ObservableLogin;
import com.smahmoodi.mvvmsample.ui.base.BaseActivity;
import com.smahmoodi.mvvmsample.ui.register.RegisterActivity;
import com.smahmoodi.mvvmsample.util.TextUtil;

public class LoginActivity extends BaseActivity<ActivityLoginBinding , LoginViewModel> implements LoginActions {

    private ObservableLogin login= new ObservableLogin();

    LoginViewModel loginViewModel;
    ActivityLoginBinding binding;


    @Override
    public int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    public LoginViewModel getViewModel() {
        return loginViewModel;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = getViewDataBinding();
        binding.setVariable(BR.form, login);
        binding.setVariable(BR.viewModel, loginViewModel);
        bind();

        loginViewModel.setNavigator(this);
    }


    @Override
    public void onFragmentAttached() {
        //do sth
    }

    @Override
    public void onFragmentDetached(String tag) {
        //do sth
    }

    public void performDependencyInjection() {
        //((ClockApp)getApplication()).getAppComponent().inject(this);
        loginViewModel = LoginViewModel.getInstance(this,new AppApiHelper());
    }

    @Override
    protected void onStop() {
        super.onStop();
    }


    @Override
    public void login() {
        if (TextUtil.isPhoneAndPasswordValid(login.getUsername() , login.getPassword())){
            hideKeyboard();
            login.setMessage("");
            loginViewModel.login(login.getUsername(),login.getPassword());

        }else {
            login.setMessage("* لطفا اطلاعات ورودی را بررسی نمائید");
        }
    }

    @Override
    public void loggedIn() {
        login.setMessageColor(R.color.green);
        login.setMessage("* شما وارد شدید! *تبریک*");
    }

    @Override
    public void handleError(Throwable throwable) {
        login.setMessageColor(R.color.yellow);
        login.setMessage(throwable.getMessage());
    }

    @Override
    public void setLoading(boolean b) {
        login.setLoading(b);
    }

    @Override
    public void toRegister() {
        startActivity(
                RegisterActivity.getIntent(this)
        );
    }

    @Override
    public void toForgetPass() {

    }
}
