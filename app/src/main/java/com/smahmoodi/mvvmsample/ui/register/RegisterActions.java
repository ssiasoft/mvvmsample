package com.smahmoodi.mvvmsample.ui.register;

public interface RegisterActions {
    void register();

    void registered();

    void handleError(Throwable throwable);

    void setLoading(boolean b);

    void toPrevActivity();

    void enterCode();

    void verifyCode();
}
