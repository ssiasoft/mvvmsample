package com.smahmoodi.mvvmsample.ui.register;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;

import com.smahmoodi.mvvmsample.BR;
import com.smahmoodi.mvvmsample.R;
import com.smahmoodi.mvvmsample.data.api.AppApiHelper;
import com.smahmoodi.mvvmsample.databinding.ActivityRegisterBinding;
import com.smahmoodi.mvvmsample.databinding.EntryDialogBinding;
import com.smahmoodi.mvvmsample.model.ObservableRegister;
import com.smahmoodi.mvvmsample.model.ObservableVerificationCode;
import com.smahmoodi.mvvmsample.ui.base.BaseActivity;
import com.smahmoodi.mvvmsample.util.GeneralUtils;
import com.smahmoodi.mvvmsample.util.TextUtil;

public class RegisterActivity extends BaseActivity<ActivityRegisterBinding , RegisterViewModel> implements RegisterActions {

    private ObservableRegister register= new ObservableRegister();
    private ObservableVerificationCode verificationCode= new ObservableVerificationCode();
    private Dialog verificationDialog ;

    RegisterViewModel registerViewModel;
    ActivityRegisterBinding binding;
    EntryDialogBinding dialogBinding;

    public static Intent getIntent(Context context){
        return new Intent(context,RegisterActivity.class);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_register;
    }

    @Override
    public RegisterViewModel getViewModel() {
        return registerViewModel;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = getViewDataBinding();
        binding.setVariable(BR.form, register);
        binding.setVariable(BR.viewModel, registerViewModel);
        bind();

        registerViewModel.setNavigator(this);
    }


    @Override
    public void onFragmentAttached() {
        //do sth
    }

    @Override
    public void onFragmentDetached(String tag) {
        //do sth
    }

    public void performDependencyInjection() {
        //((ClockApp)getApplication()).getAppComponent().inject(this);
        registerViewModel = RegisterViewModel
                .getInstance(
                        this
                        ,new AppApiHelper()
                );
    }

    @Override
    protected void onStop() {
        super.onStop();
    }


    @Override
    public void register() {
        if (TextUtil.isPhoneAndPasswordValid(register.getMobilePhoneNumber() , register.getPassword()) &&
                TextUtil.isNamesValid(register.getFirstname(),register.getLastname())){

            hideKeyboard();
            register.setMessage("");
            registerViewModel.register(register.getFirstname() ,register.getLastname(),register.getMobilePhoneNumber(),register.getPassword());

        }else {
            setMsg("* لطفا اطلاعات ورودی را بررسی نمائید" ,R.color.yellow);
            //Toast.makeText(this, "sss", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void registered() {
        verificationCode.setLoading(false);
        verificationDialog.hide();

        setLoading(false);

        setMsg("* ثبت نام انجام شد! *تبریک*" , R.color.green);

    }

    private void setMsg(String msg , int color){

        if (verificationDialog !=null)
            if (verificationDialog.isShowing()) {
                verificationCode.setLoading(false);
                verificationCode.setMessage(msg);
                verificationCode.setMessageColor(R.color.yellow);
                return;
            }

        register.setMessageColor(color);
        register.setMessage(msg);

    }

    @Override
    public void handleError(Throwable throwable) {

        setMsg(throwable.getMessage(), R.color.yellow);

        Log.e("handleError","errorr"+throwable.getMessage());
        //Toast.makeText(this, "error", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setLoading(boolean b) {
        register.setLoading(b);
    }

    @Override
    public void toPrevActivity() {
        finish();
    }

    @Override
    public void enterCode() {
        if(verificationDialog == null) {
            verificationDialog = GeneralUtils.buildEntryDialog(this);
            dialogBinding = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.entry_dialog, null, false);

            dialogBinding.setVariable(BR.form, verificationCode);
            dialogBinding.setVariable(BR.viewModel, registerViewModel);

            verificationDialog.setContentView(dialogBinding.getRoot());
        }

        verificationDialog.show();
    }

    @Override
    public void verifyCode() {
        if (verificationCode.getVerificationCode().length()==5) {
            verificationCode.setLoading(true);
            hideKeyboard();
            registerViewModel.verify(register.getMobilePhoneNumber(),verificationCode.getVerificationCode());

        }else {
            verificationCode.setMessageColor(R.color.yellow);
            verificationCode.setMessage("لطفا کد را بدرستی وارد کنید");
        }
    }

}



































