package com.smahmoodi.mvvmsample.ui.register;

import android.content.Context;

import com.smahmoodi.mvvmsample.data.AppServiceManager;
import com.smahmoodi.mvvmsample.data.api.ApiHelper;
import com.smahmoodi.mvvmsample.data.model.api.Authentication;
import com.smahmoodi.mvvmsample.data.model.api.RegisterResponse;
import com.smahmoodi.mvvmsample.data.model.api.VerificationResponse;
import com.smahmoodi.mvvmsample.ui.base.BaseViewModel;

import io.reactivex.schedulers.Schedulers;

public class RegisterViewModel extends BaseViewModel<RegisterActions> {

    private static RegisterViewModel instance;

    public static synchronized RegisterViewModel getInstance(Context context, ApiHelper apiHelper){
        if(instance == null){
            instance = new RegisterViewModel(AppServiceManager.getInstance(context,  apiHelper));
        }
        return instance;
    }


    public RegisterViewModel(com.smahmoodi.mvvmsample.data.AppService appService) {
        super(appService);
    }


    public void onServerLoginClick() {
        getNavigator().register();
    }

    public void register(String firstName, String lastName,String username, String password) {

        getNavigator().setLoading(true);
        getmCompositeDisposable().add(getAppService()
                .doServerRegisterApiCall(new Authentication.ServerRegisterRequest(firstName, lastName,username, password))
                .doOnSuccess((RegisterResponse response) -> {
                    // do nothing yet
                })
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(response -> {
                    getNavigator().setLoading(false);
                    if (response.isSuccess())
                        getNavigator().enterCode();
                    else
                        getNavigator().handleError(new Throwable(response.getMessage() + "\n" + response.getErrorMessage()));
                }, throwable -> {
                    getNavigator().setLoading(false);
                    getNavigator().handleError(throwable);
                }));
    }

    public void toPrevActivity(){
        getNavigator().toPrevActivity();
    }

    public void verifyCode(){
        getNavigator().verifyCode();
    }


    public void verify(String mobilePhoneNumber, String verificationCode) {
        getNavigator().setLoading(true);
        getmCompositeDisposable().add(getAppService()
                .doServerVerificationApiCall(new Authentication.ServerVerificationRequest(mobilePhoneNumber,verificationCode))
                .doOnSuccess((VerificationResponse response) -> {
                    // do nothing yet
                })
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(response -> {
                    getNavigator().setLoading(false);
                    if (response.isSuccess())
                        getNavigator().registered();
                    else
                        getNavigator().handleError(new Throwable(response.getErrorMessage()));
                }, throwable -> {
                    getNavigator().setLoading(false);
                    getNavigator().handleError(throwable);
                }));
    }
}
