package com.smahmoodi.mvvmsample.ui.login;

import android.content.Context;

import com.smahmoodi.mvvmsample.data.AppServiceManager;
import com.smahmoodi.mvvmsample.data.api.ApiHelper;
import com.smahmoodi.mvvmsample.data.model.api.Authentication;
import com.smahmoodi.mvvmsample.data.model.api.LoginResponse;
import com.smahmoodi.mvvmsample.ui.base.BaseViewModel;

import io.reactivex.schedulers.Schedulers;

public class LoginViewModel extends BaseViewModel<LoginActions> {

    private static LoginViewModel instance;

    public static synchronized LoginViewModel getInstance(Context context, ApiHelper apiHelper){
        if(instance == null){
            instance = new LoginViewModel(AppServiceManager.getInstance(context,  apiHelper));
        }
        return instance;
    }


    public LoginViewModel(com.smahmoodi.mvvmsample.data.AppService appService) {
        super(appService);
    }


    public void onServerLoginClick() {
        getNavigator().login();
    }

    public void login(String username, String password) {
        getNavigator().setLoading(true);
        getmCompositeDisposable().add(getAppService()
                .doServerLoginApiCall(new Authentication.ServerLoginRequest(username, password))
                .doOnSuccess((LoginResponse response) -> {
                    // do nothing yet
                })
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(response -> {
                    getNavigator().setLoading(false);
                    if (response.isSuccess())
                        getNavigator().loggedIn();
                    else
                        getNavigator().handleError(new Throwable(response.getErrorMessage()));
                }, throwable -> {
                    getNavigator().setLoading(false);
                    getNavigator().handleError(throwable);
                }));
    }

    public void toRegActivity(){
        getNavigator().toRegister();
    }
    public void toForgetPassActivity(){
        getNavigator().toForgetPass();
    }

}
