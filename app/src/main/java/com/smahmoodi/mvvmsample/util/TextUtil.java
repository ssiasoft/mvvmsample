package com.smahmoodi.mvvmsample.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextUtil {


    public static boolean isPhoneAndPasswordValid(String phone , String pass){

        return phone.length()==11 && phone.startsWith("09") && pass.length()>3;
    }

    public static boolean isNamesValid(String firstName , String lastName){
        String USERNAME_PATTERN = "^[a-zا-ی0-9_-]{3,15}$";
        Pattern pattern=Pattern.compile(USERNAME_PATTERN);

        Matcher matcher1,matcher2;

        matcher1 = pattern.matcher(firstName);
        matcher2 = pattern.matcher(lastName);
        return matcher1.matches() && matcher2.matches();
    }
}
