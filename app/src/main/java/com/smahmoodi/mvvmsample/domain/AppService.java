package com.smahmoodi.mvvmsample.domain;

import android.util.Log;

import com.smahmoodi.mvvmsample.ClockApp;

public class AppService implements AppServiceInterface {

    /*@Inject
    RegisterViewModel clockViewModel;
*/
    public AppService(ClockApp app) {
        //app.getAppComponent().inject(this);
    }

    @Override
    public void onClockUpdated(String newClock) {
        Log.e("*****","fired!!");

        //clockViewModel.onPostClockUpdated(newClock);
    }
}
