package com.smahmoodi.mvvmsample.domain;

public interface AppServiceInterface {
    public void onClockUpdated(String newClock);
}
