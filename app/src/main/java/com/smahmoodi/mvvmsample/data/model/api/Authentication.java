/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.smahmoodi.mvvmsample.data.model.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by amitshekhar on 07/07/17.
 */

public final class Authentication {

    private Authentication() {
        // This class is not publicly instantiable
    }



    public static class ServerLoginRequest {

        @Expose
        @SerializedName("mobilePhoneNumber")
        private String mobilePhoneNumber;

        @Expose
        @SerializedName("verificationCode")
        private String password;

        public ServerLoginRequest(String mobilePhoneNumber, String password) {
            this.mobilePhoneNumber = mobilePhoneNumber;
            this.password = password;
        }

        @Override
        public boolean equals(Object object) {
            if (this == object) {
                return true;
            }
            if (object == null || getClass() != object.getClass()) {
                return false;
            }

            ServerLoginRequest that = (ServerLoginRequest) object;

            if (mobilePhoneNumber != null ? !mobilePhoneNumber.equals(that.mobilePhoneNumber) : that.mobilePhoneNumber != null) {
                return false;
            }
            return password != null ? password.equals(that.password) : that.password == null;
        }

        @Override
        public int hashCode() {
            int result = mobilePhoneNumber != null ? mobilePhoneNumber.hashCode() : 0;
            result = 31 * result + (password != null ? password.hashCode() : 0);
            return result;
        }

        public String getMobilePhoneNumber() {
            return mobilePhoneNumber;
        }

        public String getPassword() {
            return password;
        }
    }

    public static class ServerRegisterRequest {

        @Expose
        @SerializedName("firstName")
        private String firstName;

        @Expose
        @SerializedName("lastName")
        private String lastName;

        @Expose
        @SerializedName("mobilePhoneNumber")
        private String mobilePhoneNumber;

        @Expose
        @SerializedName("verificationCode")
        private String password;

        public ServerRegisterRequest(String firstName, String lastName, String mobilePhoneNumber, String password) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.mobilePhoneNumber = mobilePhoneNumber;
            this.password = password;
        }

        @Override
        public boolean equals(Object object) {
            if (this == object) {
                return true;
            }
            if (object == null || getClass() != object.getClass()) {
                return false;
            }

            ServerRegisterRequest that = (ServerRegisterRequest) object;

            if (mobilePhoneNumber != null ? !mobilePhoneNumber.equals(that.mobilePhoneNumber) : that.mobilePhoneNumber != null) {
                return false;
            }
            return password != null ? password.equals(that.password) : that.password == null;
        }

        @Override
        public int hashCode() {
            int result = mobilePhoneNumber != null ? mobilePhoneNumber.hashCode() : 0;
            result = 31 * result + (password != null ? password.hashCode() : 0);
            return result;
        }

        public String getFirstName() {
            return firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public String getMobilePhoneNumber() {
            return mobilePhoneNumber;
        }

        public String getPassword() {
            return password;
        }
    }


    public static class ServerVerificationRequest {

        @Expose
        @SerializedName("mobilePhoneNumber")
        private String mobilePhoneNumber;

        @Expose
        @SerializedName("verificationCode")
        private String verificationCode;

        public ServerVerificationRequest(String mobilePhoneNumber, String verificationCode) {
            this.mobilePhoneNumber = mobilePhoneNumber;
            this.verificationCode = verificationCode;
        }

        @Override
        public boolean equals(Object object) {
            if (this == object) {
                return true;
            }
            if (object == null || getClass() != object.getClass()) {
                return false;
            }

            ServerLoginRequest that = (ServerLoginRequest) object;

            if (mobilePhoneNumber != null ? !mobilePhoneNumber.equals(that.mobilePhoneNumber) : that.mobilePhoneNumber != null) {
                return false;
            }
            return verificationCode != null ? verificationCode.equals(that.password) : that.password == null;
        }

        @Override
        public int hashCode() {
            int result = mobilePhoneNumber != null ? mobilePhoneNumber.hashCode() : 0;
            result = 31 * result + (verificationCode != null ? verificationCode.hashCode() : 0);
            return result;
        }

        public String getMobilePhoneNumber() {
            return mobilePhoneNumber;
        }

        public String getVerificationCode() {
            return verificationCode;
        }
    }

}
