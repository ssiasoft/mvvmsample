package com.smahmoodi.mvvmsample.data.api;

import com.smahmoodi.mvvmsample.data.model.api.Authentication;
import com.smahmoodi.mvvmsample.data.model.api.LoginResponse;
import com.rx2androidnetworking.Rx2AndroidNetworking;
import com.smahmoodi.mvvmsample.data.model.api.RegisterResponse;
import com.smahmoodi.mvvmsample.data.model.api.VerificationResponse;

import io.reactivex.Single;

public class AppApiHelper implements ApiHelper {

    private static AppApiHelper instance;
    public static synchronized AppApiHelper getInstance(){
        if(instance == null){
            instance = new AppApiHelper();
        }
        return instance;
    }

    public AppApiHelper() {
    }

    @Override
    public Single<LoginResponse> doServerLoginApiCall(Authentication.ServerLoginRequest request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SERVER_LOGIN)
                //.addBodyParameter(request)
                //.addHeaders(mApiHeader.getPublicApiHeader())
                //.addJSONObjectBody(request)
                .addApplicationJsonBody(request)
                .build()
                .getObjectSingle(LoginResponse.class);
    }

    @Override
    public Single<RegisterResponse> doServerRegisterApiCall(Authentication.ServerRegisterRequest request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SERVER_REGISTER)
                //.addBodyParameter(request)
                //.addHeaders(mApiHeader.getPublicApiHeader())
                //.addJSONObjectBody(request)
                .addApplicationJsonBody(request)
                .build()
                .getObjectSingle(RegisterResponse.class);
    }

    @Override
    public Single<VerificationResponse> doServerVerificationApiCall(Authentication.ServerVerificationRequest request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SERVER_VERIFY)
                //.addBodyParameter(request)
                //.addHeaders(mApiHeader.getPublicApiHeader())
                //.addJSONObjectBody(request)
                .addApplicationJsonBody(request)
                .build()
                .getObjectSingle(VerificationResponse.class);
    }

    @Override
    public ApiHeader getApiHeader() {
        return null;
    }
}
