/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.smahmoodi.mvvmsample.data;

import android.content.Context;

import com.google.gson.Gson;
import com.smahmoodi.mvvmsample.data.api.ApiHeader;
import com.smahmoodi.mvvmsample.data.api.ApiHelper;
import com.smahmoodi.mvvmsample.data.model.api.Authentication;
import com.smahmoodi.mvvmsample.data.model.api.LoginResponse;
import com.smahmoodi.mvvmsample.data.model.api.RegisterResponse;
import com.smahmoodi.mvvmsample.data.model.api.VerificationResponse;

import io.reactivex.Single;

/**
 * Created by amitshekhar on 07/07/17.
 */

public class AppServiceManager implements AppService {

    private final ApiHelper mApiHelper;

    private final Context mContext;

    private final Gson mGson = new Gson();

    private static AppServiceManager instance;

    public static synchronized AppServiceManager getInstance(Context context, ApiHelper apiHelper){
        if(instance == null){
            instance = new AppServiceManager(context,  apiHelper);
        }
        return instance;
    }

    public AppServiceManager(Context context, ApiHelper apiHelper) {
        mContext = context;
        mApiHelper = apiHelper;
    }

    @Override
    public Single<LoginResponse> doServerLoginApiCall(Authentication.ServerLoginRequest request) {
        return mApiHelper.doServerLoginApiCall(request);
    }

    @Override
    public Single<RegisterResponse> doServerRegisterApiCall(Authentication.ServerRegisterRequest request) {
        return mApiHelper.doServerRegisterApiCall(request);
    }

    @Override
    public Single<VerificationResponse> doServerVerificationApiCall(Authentication.ServerVerificationRequest request) {
        return mApiHelper.doServerVerificationApiCall(request);
    }

    @Override
    public ApiHeader getApiHeader() {
        return mApiHelper.getApiHeader();
    }
}
