package com.smahmoodi.mvvmsample;

import android.databinding.DataBinderMapper;
import android.databinding.DataBindingComponent;
import android.databinding.ViewDataBinding;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import com.smahmoodi.mvvmsample.databinding.ActivityLoginBindingImpl;
import com.smahmoodi.mvvmsample.databinding.ActivityRegisterBindingImpl;
import com.smahmoodi.mvvmsample.databinding.EntryDialogBindingImpl;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_ACTIVITYLOGIN = 1;

  private static final int LAYOUT_ACTIVITYREGISTER = 2;

  private static final int LAYOUT_ENTRYDIALOG = 3;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(3);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.smahmoodi.mvvmsample.R.layout.activity_login, LAYOUT_ACTIVITYLOGIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.smahmoodi.mvvmsample.R.layout.activity_register, LAYOUT_ACTIVITYREGISTER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.smahmoodi.mvvmsample.R.layout.entry_dialog, LAYOUT_ENTRYDIALOG);
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
        case  LAYOUT_ACTIVITYLOGIN: {
          if ("layout/activity_login_0".equals(tag)) {
            return new ActivityLoginBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_login is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYREGISTER: {
          if ("layout/activity_register_0".equals(tag)) {
            return new ActivityRegisterBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_register is invalid. Received: " + tag);
        }
        case  LAYOUT_ENTRYDIALOG: {
          if ("layout/entry_dialog_0".equals(tag)) {
            return new EntryDialogBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for entry_dialog is invalid. Received: " + tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    final int code = tag.hashCode();
    switch(code) {
      case -237232145: {
        if(tag.equals("layout/activity_login_0")) {
          return R.layout.activity_login;
        }
        break;
      }
      case 2013163103: {
        if(tag.equals("layout/activity_register_0")) {
          return R.layout.activity_register;
        }
        break;
      }
      case -369758805: {
        if(tag.equals("layout/entry_dialog_0")) {
          return R.layout.entry_dialog;
        }
        break;
      }
    }
    return 0;
  }

  @Override
  public String convertBrIdToString(int id) {
    String tmpVal = InnerBrLookup.sKeys.get(id);
    return tmpVal;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(13);

    static {
      sKeys.put(com.smahmoodi.mvvmsample.BR._all, "_all");
      sKeys.put(com.smahmoodi.mvvmsample.BR.mobilePhoneNumber, "mobilePhoneNumber");
      sKeys.put(com.smahmoodi.mvvmsample.BR.password, "password");
      sKeys.put(com.smahmoodi.mvvmsample.BR.firstname, "firstname");
      sKeys.put(com.smahmoodi.mvvmsample.BR.form, "form");
      sKeys.put(com.smahmoodi.mvvmsample.BR.color, "color");
      sKeys.put(com.smahmoodi.mvvmsample.BR.now, "now");
      sKeys.put(com.smahmoodi.mvvmsample.BR.viewModel, "viewModel");
      sKeys.put(com.smahmoodi.mvvmsample.BR.message, "message");
      sKeys.put(com.smahmoodi.mvvmsample.BR.loading, "loading");
      sKeys.put(com.smahmoodi.mvvmsample.BR.username, "username");
      sKeys.put(com.smahmoodi.mvvmsample.BR.lastname, "lastname");
      sKeys.put(com.smahmoodi.mvvmsample.BR.verificationCode, "verificationCode");
    }
  }
}
