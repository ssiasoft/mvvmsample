package com.android.databinding.library.baseAdapters;

public class BR {
        public static final int _all = 0;
        public static final int color = 1;
        public static final int firstname = 2;
        public static final int form = 3;
        public static final int lastname = 4;
        public static final int loading = 5;
        public static final int message = 6;
        public static final int mobilePhoneNumber = 7;
        public static final int now = 8;
        public static final int password = 9;
        public static final int username = 10;
        public static final int verificationCode = 11;
        public static final int viewModel = 12;
}